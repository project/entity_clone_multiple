<?php

namespace Drupal\entity_clone_multiple\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_clone_multiple\ClonerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to perform clone on an entity
 */
class CloneForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\entity_clone_multiple\ClonerManager
   */
  protected $clonerManager;

  /**
   * CaptchaExamplesForm constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Constructor.
   */
  public function __construct(ClonerManager $cloner_manager) {
    $this->clonerManager = $cloner_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_clone_multiple_cloner')
    );
  }

  public function getFormId() {
    return 'entity_clone_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $clone_settings = NULL) {
    $form_state->set('settings', $clone_settings);
    
    $form['cloner'] = [
      '#title' => $this->t('Repeat'),
      '#type' => 'radios',
      '#options' => $this->getClonerOptions(),
      '#required' => TRUE,
    ];

    $form['until'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Until'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  public function getClonerOptions() {
    $cloner_definitions = $this->clonerManager->getDefinitions();
    $options = [];
    foreach ($cloner_definitions as $id => $cloner) {
      $options[$id] = $cloner['title'];
    }

    return $options;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Datetime\DrupalDateTime $until */
    $until =  $form_state->getValue('until');

    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $clone_settings = $form_state->get('settings');

    /** @var \Drupal\entity_clone_multiple\EntityClonerInterface $cloner */
    $cloner = $this->clonerManager->createInstance($form_state->getValue('cloner'));

    $entity = \Drupal::routeMatch()->getParameter($clone_settings->getType());

    if ($entity) {
      $clones = $cloner->clone($entity, $clone_settings, $until->getTimestamp());
      if (!empty($clones)) {
        foreach ($clones as $clone) {
          $clone->save();
          $this->messenger()->addStatus($this->t('Created clone <a href="@url"><b>@label</b></a>', ['@label' => $clone->label(), '@url' => $clone->toUrl()->toString()]));
        }
        $this->messenger()->addStatus($this->t('Created @number number of clones of @label', ['@number' => count($clones), '@label' => $entity->label()]));
      }
      else {
        $this->messenger()->addError($this->t('Cloning yieled no clones for @label', ['@label' => $entity->label()]));
      }
    }
    else {
      $this->messenger()->addError($this->t('Could not determine entity to be cloned'));
    }
  }
}