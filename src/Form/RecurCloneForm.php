<?php

namespace Drupal\entity_clone_multiple\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_clone_multiple\ClonerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Config\Config;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Form to perform clone on an entity
 */
class RecurCloneForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\entity_clone_multiple\ClonerManager
   */
  protected $clonerManager;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * CaptchaExamplesForm constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Constructor.
   * @param \Drupal\Core\Config\Config $config
   *   The 'menu_test.links.action' config.
   */
  public function __construct(ClonerManager $cloner_manager, Config $config, DateFormatterInterface $date_formatter) {
    $this->clonerManager = $cloner_manager;
    $this->config = $config;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_clone_multiple_cloner'),
      $container->get('config.factory')->get('entity_clone_multiple.settings'),
      $container->get('date.formatter'),
    );
  }

  public function getFormId() {
    return 'entity_recur_clone_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $clone_settings = NULL) {
    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $form_state->set('settings', $clone_settings);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = \Drupal::routeMatch()->getParameter($clone_settings->getType());

    if (!$entity) {
      // Not to build form if entity unavailable.
      return [];
    }

    $form_state->set('entity', $entity);

    $step = $form_state->get('step') ?? 'first';

    $date_field_value = $clone_settings->getRecurFieldValue($entity);
    // $timezone = $date_field_value->getTimezone();

    $entity_datetime = $this->dateFormatter->format($date_field_value->getTimestamp(), $this->config->get('datetime_format'));
    $form['entity_date_info'] = array(
      '#markup' => $this->t('<div>The recurring date and time for cloning will be based on the value from the %field field which is: @datetime</div>', ['%field' => $clone_settings->getRecurFieldLabel(), '@datetime' => $entity_datetime])
    );

    if ($step == 'first') {
      $form_state->set('step', 'first');
    
      $form['recur_option'] = [
        '#type' => 'radios',
        '#title' => $this->t('Recur Option'),
        '#options' => [
          'days' => $this->t('Pick days of the week'),
          'rules' => $this->t('Every day, every 2 weeks, etc...'),
        ],
        '#default_value' => $form_state->get('recur_option') ?? 'days',
        '#required' => TRUE,
      ];

      $form['days'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Days of the Week'),
        '#options' => [
          'monday' => $this->t('Monday'),
          'tuesday' => $this->t('Tuesday'),
          'wednesday' => $this->t('Wednesday'),
          'thursday' => $this->t('Thursday'),
          'friday' => $this->t('Friday'),
          'saturday' => $this->t('Saturday'),
          'sunday' => $this->t('Sunday'),
        ],
        '#states' => [
          'visible' => [
            ':input[name="recur_option"]' => ['value' => 'days']
          ],
        ],
        '#default_value' => $form_state->get('days') ?? [],
      ];

      $form['rules'] = [
        '#type' => 'fieldset',
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="recur_option"]' => ['value' => 'rules']
          ],
        ],
      ];
    
      $options = [];
      for ($i = 1; $i < 11; $i++) {
        $options[$i] = $this->t('Every @i', ['@i' => ($i == 1) ? '' : $i]);
      }
      $form['rules']['frequency'] = [
        '#type' => 'select',
        '#title' => t('Repeat'),
        '#options' => $options,
        '#default_value' => $form_state->get('frequency') ?? NULL,
      ];
    
      $form['rules']['period'] = array(
        '#type' => 'select',
        '#options' => [
          'day' => $this->t('Day(s)'),
          'week' => $this->t('Week(s)'),
          'month' => $this->t('Month(s)'),
        ],
        '#default_value' => $form_state->get('period') ?? [],
      );
    
      $form['rules']['exclude_weekends'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('Exclude weekends'),
        '#description' => $this->t('If checked, weekends will not be included.'),
        '#default_value' => $form_state->get('exclude_weekends') ?? FALSE,
      );

      $form['until'] = [
        '#type' => 'date',
        '#title' => $this->t('Recur Until'),
        '#required' => TRUE,
        '#default_value' => $form_state->get('until'),
      ];

      $form['submit_help'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => $this->t('By pressing this button, you will get list of dates which you can confirm to perform actual cloning of the content.')
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Get Dates'),
      ];
    }
    else if ($step == 'confirm') {
      // Display the dates to the user
      $form['message'] = array(
        '#markup' => t('The following dates will be generated. Please confirm them before continuing.'),
      );
      $dates = $this->generateDates($form, $form_state);
      $form_state->set('dates', $dates);
      $date_strings = [];
      foreach ($dates as $timestamp) {
        $date_strings[] = $this->dateFormatter->format($timestamp, $this->config->get('datetime_format'));
      }

      $form['list'] = [
        '#theme' => 'item_list',
        '#items' => $date_strings,
      ];

      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Confirm'),
        '#button_type' => 'primary',
        '#name' => 'confirm',
      ];
      $form['actions']['back'] = [
        '#type' => 'submit',
        '#value' => $this->t('Go Back'),
        '#button_type' => 'secondary',
        '#name' => 'back',
      ];
    }

    return $form;
  }


  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();

    $step = $form_state->get('step');
    if ($step == 'first') {
      // Go to next step.
      $form_state->set('step', 'confirm');
      $form_state->setRebuild(TRUE);
      $this->saveUserChoices($form, $form_state);
    }
    else if ($step == 'confirm') {
      if ($trigger['#name'] == 'back') {
        $form_state->set('step', 'first');
        $form_state->setRebuild(TRUE);
      }
      else {
        /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
        $clone_settings = $form_state->get('settings');
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $form_state->get('entity');

        $dates = $form_state->get('dates');

        $clones = [];
        foreach ($dates as $timestamp) {
          $clone = $clone_settings->cloneEntity($entity, DrupalDateTime::createFromTimestamp($timestamp));
          $clone->save();
          $clones[] = $clone;
          $this->messenger()->addStatus($this->t('Created clone <a href="@url"><b>@label</b></a>', ['@label' => $clone->label(), '@url' => $clone->toUrl()->toString()]));
        }
        if (!empty($clones)) {
          $this->messenger()->addStatus($this->t('Created @number number of clones of @label', ['@number' => count($clones), '@label' => $entity->label()]));
        }
      }
    }
  }

  public function generateDates(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_state->get('entity');
    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $clone_settings = $form_state->get('settings');

    $start_date = $clone_settings->getRecurFieldValue($entity);
    $until = new DrupalDateTime($form_state->getValue('until'));
    // Set time to last moment of the day to inlcude the until day in date generation.
    $until->setTime(23, 59, 59);
    
    if ($form_state->getValue('recur_option') == 'days') {
      $days = array_filter($form_state->getValue('days'));
      return $this->generateDatesByDaysOfWeek($start_date, $days, $until);
    }
    else if ($form_state->getValue('recur_option') == 'rules') {
      $frequency = $form_state->getValue('frequency');
      $period = $form_state->getValue('period');
      $exclude_weekends = (bool) $form_state->getValue('exclude_weekends');
      return $this->generateDatesByRules($start_date, $frequency, $period, $exclude_weekends, $until);
    }
  }

  /**
   * Logic from function entity_recur_generate_dates_days() of D7 entity_recur module.
   */
  public function generateDatesByDaysOfWeek(DrupalDateTime $start_date, $days, DrupalDateTime $until) {
    $dates = [];

    // Determine which day to start with which would be the closest
    // "next" day, ie, depending on the initial date, next friday
    // may be sooner than next monday.
    $day_timestamps = array();
    foreach ($days as $day) {
      // Track the current date
      $current = clone $start_date;
      // Find timestamp for next "day" and keep in array.
      $current->modify("next {$day}");
      $day_timestamps[$day] = $current->getTimestamp();
    }
    // Sort days in order of occurance.
    asort($day_timestamps, SORT_NUMERIC);
    $ordered_days = array_keys($day_timestamps);

    $current = clone $start_date;

    // Iterate and generate dates until we reach the end
    while (TRUE) {
      foreach ($ordered_days as $day) {
        // Determine how many days until the next "day"
        $current->modify("next {$day}");
        // Make sure the hours match, to avoid DST issue
        $current->setTime($start_date->format('G'), intval($start_date->format('i')), intval($start_date->format('s')));

        // See if this date puts us past the limit
        if ($current->getTimestamp() > $until->getTimestamp()) {
          break 2;
        }

        $dates[] = $current->getTimestamp();
      }
    }

    return $dates;
  }

  /**
   * Logic from function entity_recur_generate_dates_rule() of D7 entity_recur module.
   */
  public function generateDatesByRules(DrupalDateTime $start_date, $frequency, $period, $exclude_weekends, DrupalDateTime $until) {
    $dates = array();
    $month = FALSE;

    // Convert month period to weeks, in order to preserve the day
    // of the week
    if ($period == 'month') {
      $frequency = $frequency * 4;
      $period = 'week';
      $month = TRUE;
    }

    // Track the current date
    $current = clone $start_date;

    // Iterate and generate dates until we reach the end
    while (TRUE) {
      $orignal_month = $current->format('n');
      // Generate the next date
      /** @var \Drupal\Core\Datetime\DrupalDateTime $next */
      $current->modify("+{$frequency} " . $this->formatPlural($frequency, $period, "{$period}s"));

      // If this is a month recur, we need to make sure the the next date
      // is on the next month. Some months have 5 repeats of the same day
      if ($month && ($current->format('n') == $orignal_month)) {
        // Jump forward one more week
        $current->modify('+1 week');
      }


      // If we're excluding weekends, skip this if it's a weekend
      if (($current->format('N') == 6 || $current->format('N') == 7) && $exclude_weekends) {
        continue;
      }

      // See if this date puts us past the limit
      if ($current > $until) {
        break;
      }

      $dates[] = $current->getTimestamp();
    }

    return $dates;
  }

  /**
   * Save choices user made in first step. So, it can be used to rebuild the selection if user Go Back to first step.
   */
  public function saveUserChoices(array &$form, FormStateInterface $form_state) {
    $form_state->set('recur_option', $form_state->getValue('recur_option'));
    $form_state->set('days', $form_state->getValue('days'));

    $form_state->set('frequency', $form_state->getValue('frequency'));
    $form_state->set('period', $form_state->getValue('period'));
    $form_state->set('exclude_weekends', $form_state->getValue('exclude_weekends'));

    $form_state->set('until', $form_state->getValue('until'));
  }
}