<?php

namespace Drupal\entity_clone_multiple\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Entity Clone Multiple settings admin form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The date format entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateFormatStorage;

  /**
   * Constructs the class for MHCC License form.
   */
  public function __construct(ConfigFactoryInterface $config_factory, DateFormatterInterface $date_formatter, EntityStorageInterface $date_format_storage) {
    parent::__construct($config_factory);
    $this->dateFormatter = $date_formatter;
    $this->dateFormatStorage = $date_format_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('date_format')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_clone_multiple_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_clone_multiple.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('entity_clone_multiple.settings');

    // $form['dateinterval_durations'] = [
    //   '#type' => 'textarea',
    //   '#title' => $this->t('DateInterval Cloner Durations'),
    //   '#default_value' => $config->get('dateinterval_durations'),
    //   '#description' => $this->t('Enter available durations. Each line needs to have three parts separated by pipe character, “|” where first part is the duration to pass to <a href="https://www.php.net/manual/en/dateinterval.construct.php">DateInterval class</a>, second part is the title and third part is the description.')
    // ];

    $time = new DrupalDateTime();
    $format_types = $this->dateFormatStorage->loadMultiple();
    $options = [];
    foreach ($format_types as $type => $type_info) {
      $format = $this->dateFormatter->format($time->getTimestamp(), $type);
      $options[$type] = $type_info->label() . ' (' . $format . ')';
    }

    $form['datetime_format'] = [
      '#title' => $this->t('Datetime format'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $config->get('datetime_format'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_clone_multiple.settings');
    $config
      // ->set('dateinterval_durations', $form_state->getValue('dateinterval_durations'))
      ->set('datetime_format', $form_state->getValue('datetime_format'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
