<?php

namespace Drupal\entity_clone_multiple\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_clone_multiple\ClonerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Config\Config;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Form to perform clone on an entity
 */
class RecurCloneAdvancedForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\entity_clone_multiple\ClonerManager
   */
  protected $clonerManager;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * CaptchaExamplesForm constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Constructor.
   * @param \Drupal\Core\Config\Config $config
   *   The 'menu_test.links.action' config.
   */
  public function __construct(ClonerManager $cloner_manager, Config $config, DateFormatterInterface $date_formatter, ModuleHandlerInterface $module_handler) {
    $this->clonerManager = $cloner_manager;
    $this->config = $config;
    $this->dateFormatter = $date_formatter;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_clone_multiple_cloner'),
      $container->get('config.factory')->get('entity_clone_multiple.settings'),
      $container->get('date.formatter'),
      $container->get('module_handler')
    );
  }

  public function getFormId() {
    return 'entity_recur_clone_advanced_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $clone_settings = NULL) {
    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $form_state->set('settings', $clone_settings);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = \Drupal::routeMatch()->getParameter($clone_settings->getType());

    if (!$entity) {
      // Not to build form if entity unavailable.
      return [];
    }

    $form_state->set('entity', $entity);

    $step = $form_state->get('step') ?? 'first';

    $date_field_value = $clone_settings->getRecurFieldValue($entity);
    $entity_datetime = $this->dateFormatter->format($date_field_value->getTimestamp(), $this->config->get('datetime_format'));

    $form['entity_date_info'] = array(
      '#markup' => $this->t('<p>The recurring date and time for cloning will be based on the value from the %field field which is: @datetime</p>', ['%field' => $clone_settings->getRecurFieldLabel(), '@datetime' => $entity_datetime])
    );

    if ($step == 'first') {
      $form_state->set('step', 'first');
    
      $form['recur_option'] = [
        '#type' => 'radios',
        '#title' => $this->t('Recur'),
        '#options' => [
          'daily' => $this->t('Daily'),
          'weekly' => $this->t('Weekly'),
          'monthly' => $this->t('Monthly'),
        ],
        '#default_value' => $form_state->get('recur_option') ?? 'daily',
        '#required' => TRUE,
      ];

      $form['daily_frequency'] = [
        '#title' => $this->t('Frequency'),
        '#type' => 'number',
        '#size' => 3,
        '#step' => 1,
        '#field_prefix' => $this->t('Every '),
        '#field_suffix' => $this->t(' days'),
        '#min' => 1,
        '#max' => 365,
        '#size' => 5,
        '#maxlength' => 5,
        '#default_value' => $form_state->get('daily_frequency') ?? 1,
        '#states' => [
          'visible' => [
            ':input[name="recur_option"]' => ['value' => 'daily']
          ],
        ],
      ];

      $form['weekly'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Weekly Options'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="recur_option"]' => ['value' => 'weekly']
          ],
        ],
      ];

      $weekly_frequency_options = [];
      for ($i = 1; $i < 5; $i++) {
        $weekly_frequency_options[$i] = $this->t('Every @i @weeks', ['@i' => ($i == 1) ? '' : $i, '@weeks' => $this->formatPlural($i, 'week', "weeks")]);
      }

      $form['weekly']['weekly_frequency'] = [
        '#title' => $this->t('Frequency'),
        '#type' => 'radios',
        '#options' => $weekly_frequency_options,
        '#default_value' => $form_state->get('weekly_frequency') ?? 1,
      ];

      $week_days_options = [
        'monday' => $this->t('Monday'),
        'tuesday' => $this->t('Tuesday'),
        'wednesday' => $this->t('Wednesday'),
        'thursday' => $this->t('Thursday'),
        'friday' => $this->t('Friday'),
        'saturday' => $this->t('Saturday'),
        'sunday' => $this->t('Sunday'),
      ];

      $form['weekly']['weekly_days'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Days of the Week'),
        '#options' => $week_days_options,
        '#default_value' => $form_state->get('weekly_days') ?? [strtolower($this->dateFormatter->format($date_field_value->getTimestamp(), 'custom', 'l'))],
        // '#description' => $this->t('Uncheck to skip generating for those week days.'),
      ];

      $form['monthly'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Monthly Options'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="recur_option"]' => ['value' => 'monthly']
          ],
        ],
      ];

      $form['monthly']['monthly_option'] = [
        '#type' => 'radios',
        '#title' => $this->t('Date or Day'),
        '#options' => [
          'same_date' => $this->t('Same date (@date day of the month)', ['@date' => $date_field_value->format('jS')]),
          'same_day' => $this->t('Same day (@th @day_of_week)', ['@th' => static::getNumberWithOrdinalSuffix(intdiv($date_field_value->format('j'), 7) +1), '@day_of_week' => $date_field_value->format('l') ]),
        ],
        '#default_value' => $form_state->get('monthly_option') ?? 'same_date',
        '#description' => $this->t('If Date option selected then it will happen on same date, that means if original date is 4th of the month then generated dates will be 4th of upcoming months. If Day option is selected then it will happen on same weekday, for example if original date is on 2nd Saturday on the month then it will generate dates happening on 2nd Saturday of upcoming months.')
      ];

      $monthly_frequency_options = [];
      for ($i = 1; $i < 13; $i++) {
        $monthly_frequency_options[$i] = $this->t('Every @i @months', ['@i' => ($i == 1) ? '' : $i, '@months' => $this->formatPlural($i, 'month', "months")]);
      }

      $form['monthly']['monthly_frequency'] = [
        '#title' => $this->t('Frequency'),
        '#type' => 'select',
        '#options' => $monthly_frequency_options,
        '#default_value' => $form_state->get('monthly_frequency') ?? 1,
      ];

      if ($date_field_value->format('j') > 28) {
        $form['monthly']['monthly_option_same_date_warning'] = [
          '#type' => 'item',
          '#markup' => $this->t('<p>Months not having @date day will be skipped.</p>', ['@date' => $date_field_value->format('jS')]),
          '#states' => [
            'visible' => [
              ':input[name="monthly_option"]' => ['value' => 'same_date']
            ],
          ],
        ];
      }

      if ( (intdiv($date_field_value->format('j'), 7) +1) > 4) {
        $form['monthly']['monthly_option_same_week_day_warning'] = [
          '#type' => 'item',
          '#markup' => $this->t('<p>Months not having @th @day_of_week will be skipped.</p>', ['@th' => static::getNumberWithOrdinalSuffix(intdiv($date_field_value->format('j'), 7) +1), '@day_of_week' => $date_field_value->format('l') ]),
          '#states' => [
            'visible' => [
              ':input[name="monthly_option"]' => ['value' => 'same_day']
            ],
          ],
        ];
      }

      $form['until'] = [
        '#type' => 'date',
        '#title' => $this->t('Recur Until'),
        '#required' => TRUE,
        '#default_value' => $form_state->get('until'),
      ];

      if ($this->moduleHandler->moduleExists('content_moderation')) {
        $moderation_information = \Drupal::service('content_moderation.moderation_information');

        if ($moderation_information->isModeratedEntity($entity)) {
          if ($this->moduleHandler->moduleExists('content_moderation_permissions')) {
            $moderation_sate_transition_validator = \Drupal::service('content_moderation_permissions.state_transition_validation');
          }
          else {
            $moderation_sate_transition_validator = \Drupal::service('content_moderation.state_transition_validation');
          }

          $temp_clone = $clone_settings->cloneEntity($entity, DrupalDateTime::createFromTimestamp(time()));
          $current_user = \Drupal::service('current_user');


          /** @var \Drupal\workflows\Transition[] $transitions */
          $transitions = $moderation_sate_transition_validator->getValidTransitions($temp_clone, $current_user);

          $default_state = $moderation_information->getOriginalState($temp_clone);

          $transition_labels = [];
          $default_value = $temp_clone->get('moderation_state')->value;
          foreach ($transitions as $transition) {
            $transition_to_state = $transition->to();
            $transition_labels[$transition_to_state->id()] = $transition_to_state->label();
            if ($default_state->id() === $transition_to_state->id()) {
              $default_value = $default_state->id();
            }
          }

          $form['moderation_state_container'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Content Moderation'),
          ];

          $form['moderation_state_container']['moderation_state'] = [
            '#type' => 'select',
            '#title' => $this->t('Set State As'),
            // '#key_column' => $this->column,
            '#options' => $transition_labels,
            '#default_value' => $form_state->get('moderation_state') ?? $default_value,
            '#access' => !empty($transition_labels),
            '#wrapper_attributes' => [
              'class' => ['container-inline'],
            ],
            '#description' => $this->t('Select moderation state to set for cloned content.'),
          ];
        }
      }

      $form['submit_help'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => $this->t('<p>By pressing this button, you will get list of dates which you can confirm to perform actual cloning of the content.</p>')
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Get Dates'),
      ];
    }
    else if ($step == 'confirm') {
      // Display the dates to the user
      $form['message'] = array(
        '#markup' => t('<p>The following dates will be generated. Please confirm them before continuing. This cannot be undone without editing every new instance of the content.</p>'),
      );
      $dates = $this->generateDates($form, $form_state);
      $form_state->set('dates', $dates);
      $date_strings = [];
      foreach ($dates as $timestamp) {
        $date_strings[] = $this->dateFormatter->format($timestamp, $this->config->get('datetime_format'));
      }

      $form['list'] = [
        '#theme' => 'item_list',
        '#items' => $date_strings,
      ];

      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Confirm'),
        '#button_type' => 'primary',
        '#name' => 'confirm',
      ];
      $form['actions']['back'] = [
        '#type' => 'submit',
        '#value' => $this->t('Go Back'),
        '#button_type' => 'secondary',
        '#name' => 'back',
      ];
    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $step = $form_state->get('step');
    if ($step == 'first') {
      if (!preg_match('/^\d+$/', $form_state->getValue('daily_frequency'))) {
        $form_state->setErrorByName('daily_frequency', $this->t('Please enter a valid number.'));
      }
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();

    $step = $form_state->get('step');
    if ($step == 'first') {
      // Go to next step.
      $form_state->set('step', 'confirm');
      $form_state->setRebuild(TRUE);
      $this->saveUserChoices($form, $form_state);
    }
    else if ($step == 'confirm') {
      if ($trigger['#name'] == 'back') {
        $form_state->set('step', 'first');
        $form_state->setRebuild(TRUE);
      }
      else {
        /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
        $clone_settings = $form_state->get('settings');
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $form_state->get('entity');

        $dates = $form_state->get('dates');

        $clones = [];
        foreach ($dates as $timestamp) {
          $clone = $clone_settings->cloneEntity($entity, DrupalDateTime::createFromTimestamp($timestamp));
          $moderation_state = $form_state->get('moderation_state');
          if (!empty($moderation_state)) {
            $clone->set('moderation_state', $moderation_state);
          }
          $clone->save();
          $clones[] = $clone;
          $this->messenger()->addStatus($this->t('Created clone <a href="@url"><b>@label</b></a>', ['@label' => $clone->label(), '@url' => $clone->toUrl()->toString()]));
        }
        if (!empty($clones)) {
          $this->messenger()->addStatus($this->t('Created @number number of clones of @label', ['@number' => count($clones), '@label' => $entity->label()]));
        }
      }
    }
  }

  public function generateDates(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_state->get('entity');
    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $clone_settings = $form_state->get('settings');

    $start_date = $clone_settings->getRecurFieldValue($entity);
    $start_date->setTimezone(new \DateTimeZone(date_default_timezone_get()));
    $until = new DrupalDateTime($form_state->getValue('until'));
    // Set time to last moment of the day to inlcude the until day in date generation.
    $until->setTime(23, 59, 59);

    $dates = [];
    if ($form_state->get('recur_option') == 'daily') {
      $dates = $this->generateDatesByDaysOfWeek($start_date, $form_state->get('daily_frequency'), $until);
    }
    else if ($form_state->get('recur_option') == 'weekly') {
      $dates = $this->generateDatesWeekly($start_date, $form_state->get('weekly_frequency'), $form_state->get('weekly_days'), $until);
    }
    else if ($form_state->get('recur_option') == 'monthly') {
      $dates = $this->generateDatesMonthly($start_date, $form_state->get('monthly_option'), $form_state->get('monthly_frequency'), $until);
    }
    return $dates;
  }

  /**
   */
  public function generateDatesByDaysOfWeek(DrupalDateTime $start_date, $daily_frequency, DrupalDateTime $until) {
    $dates = [];

    $current = clone $start_date;

    // Iterate and generate dates until we reach the end
    while (TRUE) {
      $modification = '+' . $daily_frequency . ' ' . $this->formatPlural($daily_frequency, 'day', "days");
      $current->modify($modification);
      // See if this date puts us past the limit
      if ($current->getTimestamp() > $until->getTimestamp()) {
        break;
      }

      $dates[] = $current->getTimestamp();
    }

    return $dates;
  }

  /**
   */
  public function generateDatesWeekly(DrupalDateTime $start_date, $weekly_frequency, $week_days, DrupalDateTime $until) {
    if (empty($week_days)) {
      return [];
    }

    $dates = [];

    $current = clone $start_date;

    $day_of_source_date = strtolower($this->dateFormatter->format($start_date->getTimestamp(), 'custom', 'l'));

    if (count($week_days) == 1 && reset($week_days) == $day_of_source_date) {
      // User just wants to repeate for same day of the week.

      // Iterate and generate dates until we reach the end
      while (TRUE) {
        $modification = '+' . $weekly_frequency . ' ' . $this->formatPlural($weekly_frequency, 'week', "weeks");
        $current->modify($modification);
        // See if this date puts us past the limit
        if ($current->getTimestamp() > $until->getTimestamp()) {
          break;
        }

        $dates[] = $current->getTimestamp();
      }
    }
    else {
      $original_week_day_number = $current->format('w');

      $week_day_numbers = [
        'sunday' => 0,
        'monday' => 1,
        'tuesday' => 2,
        'wednesday' => 3,
        'thursday' => 4,
        'friday' => 5,
        'saturday' => 6,
      ];

      while (TRUE) {
        // First advance to next week per selected frequency
        $modification = '+' . $weekly_frequency . ' ' . $this->formatPlural($weekly_frequency, 'week', "weeks");
        $current->modify($modification);
        // $dates[] = $current->getTimestamp();
        foreach ($week_days as $week_day) {
          $week_date = clone $current;
          if ($week_day_numbers[$week_day] < $original_week_day_number) {
            // Go backward in weekday as current weekday is before original weekday
            $week_date->modify('-' . $original_week_day_number - $week_day_numbers[$week_day] . ' days');
          }
          else if ($week_day_numbers[$week_day] > $original_week_day_number) {
            // Go forward in weekday as current weekday is after original weekday
            $week_date->modify('+' . $week_day_numbers[$week_day] + $original_week_day_number  . ' days');
          }
          else {
            // Nothing to do as same weekday.
          }

          if ($week_date->getTimestamp() > $until->getTimestamp()) {
            break 2;
          }

          $dates[] = $week_date->getTimestamp();
        }
      }
    }

    return $dates;
  }

  /**
   */
  public function generateDatesMonthly(DrupalDateTime $start_date, $monthly_option, $monthly_frequency, DrupalDateTime $until) {
    $dates = [];

    $current = clone $start_date;

    $day_of_source_date = strtolower($this->dateFormatter->format($start_date->getTimestamp(), 'custom', 'l'));

    if ($monthly_option == 'same_date') {
      $original_day_of_month = $current->format('j');
      // Iterate and generate dates until we reach the end
      while (TRUE) {
        // $modification = '+' . $monthly_frequency . ' ' . $this->formatPlural($monthly_frequency, 'month', 'months');
        // $current->modify($modification);
        for ($i = 1; $i <= $monthly_frequency; $i++) {
          $current->modify('first day of next month');
        }

        if ($current->format('t') < $original_day_of_month) {
          // This month don't have that date.
          continue;
        }
        else {
          $current->add(new \DateInterval('P' . ($original_day_of_month - 1)  . 'D'));
        }

        // See if this date puts us past the limit
        if ($current->getTimestamp() > $until->getTimestamp()) {
          break;
        }

        $dates[] = $current->getTimestamp();
      }
    }
    else if ($monthly_option == 'same_day') {
      $position_of_week_day = intdiv($current->format('j'), 7);
      $day_of_the_week = $current->format('l');

      while (TRUE) {
        for ($i = 1; $i <= $monthly_frequency; $i++) {
          $current->modify('first day of next month');
        }

        $temp_current = clone $current;

        $current_month_name = $temp_current->format('F');

        // Advance to the week position.
        if ($position_of_week_day > 0) {
          $temp_current->add(new \DateInterval('P' . ($position_of_week_day * 7)  . 'D'));
        }

        // Then advance to the weekday as necessary.
        if ($temp_current->format('l') != $day_of_the_week) {
          do {
            $temp_current->modify("+1 day");
          } while ($temp_current->format('l') != $day_of_the_week);
        }

        if ($temp_current->format('F') != $current_month_name) {
          // It jumped to next month.
          continue;
        }

        // See if this date puts us past the limit
        if ($temp_current->getTimestamp() > $until->getTimestamp()) {
          break;
        }

        $dates[] = $temp_current->getTimestamp();
      }
    }

    return $dates;
  }

  /**
   * Save choices user made in first step. So, it can be used to rebuild the selection if user Go Back to first step.
   */
  public function saveUserChoices(array &$form, FormStateInterface $form_state) {
    $form_state->set('recur_option', $form_state->getValue('recur_option'));
    $form_state->set('daily_frequency', $form_state->getValue('daily_frequency'));

    $form_state->set('weekly_frequency', $form_state->getValue('weekly_frequency'));
    $form_state->set('weekly_days', array_filter($form_state->getValue('weekly_days')));

    $form_state->set('monthly_option', $form_state->getValue('monthly_option'));
    $form_state->set('monthly_frequency', $form_state->getValue('monthly_frequency'));

    $form_state->set('until', $form_state->getValue('until'));

    if (isset($form['moderation_state_container']['moderation_state'])) {
      $form_state->set('moderation_state', $form_state->getValue('moderation_state'));
    }
  }

  public static function getNumberWithOrdinalSuffix($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if (($number %100) >= 11 && ($number%100) <= 13) {
      $abbreviation = $number. 'th';
    }
    else {
      $abbreviation = $number. $ends[$number % 10];
    }
    return $abbreviation;
  }
}