<?php

namespace Drupal\entity_clone_multiple\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the EntityCloneSetting configuration entity.
 *
 * @ConfigEntityType(
 *   id = "entity_clone_entity_setting",
 *   label = @Translation("Entity Clone Setting"),
 *   label_collection = @Translation("Entity Clone Settings"),
 *   label_singular = @Translation("Entity Clone Setting"),
 *   label_plural = @Translation("Entity Clone Settings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count clone setting",
 *     plural = "@count clone settings",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity_clone_multiple\EntityCloneSettingAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\entity_clone_multiple\EntityCloneSettingForm",
 *       "edit" = "Drupal\entity_clone_multiple\EntityCloneSettingForm",
 *       "delete" = "Drupal\entity_clone_multiple\Form\EntityCloneSettingDeleteConfirm"
 *     },
 *     "list_builder" = "Drupal\entity_clone_multiple\EntityCloneSettingListBuilder",
 *   },
 *   admin_permission = "administer entity clone multiple settings",
 *   config_prefix = "entity_clone_entity_setting",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/entity-clone/manage/{entity_clone_entity_setting}",
 *     "delete-form" = "/admin/structure/entity-clone/manage/{entity_clone_entity_setting}/delete",
 *     "entity-permissions-form" = "/admin/structure/entity-clone/manage/{entity_clone_entity_setting}/permissions",
 *     "collection" = "/admin/structure/entity-clone",
 *   },
 *   config_export = {
 *     "id",
 *     "entity_type",
 *     "bundle",
 *     "recur_field",
 *     "excluded_fields",
 *   }
 * )
 */
class EntityCloneSetting extends ConfigEntityBase {

  /**
   * The unique ID for the setting.
   *
   * A cobination of entity type and bundle.
   *
   * @var string
   */
  protected $id;

  /**
   * The entity type of this settings.
   *
   * @var string
   */
  protected $entity_type;

  /**
   * The entity bundle of this settings.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The recur date field.
   *
   * @var string
   */
  protected $recur_field;

  /**
   * The list of field to be excluded from clonning.
   *
   * @var array
   */
  protected $excluded_fields;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
    // return $this->getType() . '__' . $this->getBundle();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $bundle_info = \Drupal::service("entity_type.bundle.info")->getBundleInfo($this->getType())[$this->getBundle()];
    $entity_type_definition = $this->entityTypeManager()->getDefinition($this->getType());
    return t(':bundle of :entity_type entity type', [':entity_type' => $entity_type_definition->getLabel(), ':bundle' => $bundle_info['label']]);
  }

  public function getType() {
    return $this->entity_type;
  }

  public function getBundle() {
    return $this->bundle;
  }

  public function getRecurField() {
    return $this->recur_field;
  }

  public function getRecurFieldLabel() {
    $fields = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties(['entity_type' => $this->entity_type, 'bundle' => $this->bundle, 'field_name' => $this->recur_field]);
    if (!empty($fields)) {
      $field = reset($fields);
      return $field->label();
    }
  }

  public function getExcludedFields() {
    return $this->excluded_fields;
  }

  public function getRecurFieldValue(ContentEntityInterface $entity) : DrupalDateTime {
    return $entity->{$this->recur_field}->date;
  }

  public function setRecurFieldValue(ContentEntityInterface $entity, DrupalDateTime $new_value) {
    $fields = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties(['entity_type' => $this->entity_type, 'bundle' => $this->bundle, 'field_name' => $this->recur_field]);
    if (!empty($fields)) {
      $field = reset($fields);
      $format = $field->getSetting('datetime_type') == DateTimeItem::DATETIME_TYPE_DATE ? DateTimeItemInterface::DATE_STORAGE_FORMAT : DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
      $entity->set($this->recur_field, $new_value->format($format));
    }
  }

  public function clearExcludedFields(ContentEntityInterface $entity) {
    foreach (array_filter($this->excluded_fields) as $excluded_field) {
      $entity->set($excluded_field, []);

      if ($entity instanceof \Drupal\node\NodeInterface) {
        if ($excluded_field == 'created') {
          $entity->set($excluded_field, time());
        }
        else if ($excluded_field == 'status') {
          $entity->set($excluded_field, FALSE);
        }
      }
    }
  }

  public function cloneEntity($entity, DrupalDateTime $new_recur_field_value) : ContentEntityInterface {
    $clone = $entity->createDuplicate();
    $this->setRecurFieldValue($clone, $new_recur_field_value);
    $this->clearExcludedFields($clone);
    // if (!in_array('moderation_state', $this->excluded_fields) && $entity->get('moderation_state') && $entity->get('moderation_state')->value) {
    //   $clone->set('moderation_state', $entity->get('moderation_state')->value);
    // }
    return $clone;
  }

  /**
   * {@inheritdoc}
   */
  // public function label() {
  //   return $this->entity_type . ':' . $this->bundle;
  // }

  public function buildForm() {
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\entity_clone_multiple\Form\RecurCloneAdvancedForm', $this);
    return $build;
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *  Entity
   * @return static|null
   *   The entity object or NULL if there is no clone settings for the given entity.
   */
  public static function loadForEntity(ContentEntityInterface $entity) {
    $clone_settings_storage = \Drupal::entityTypeManager()->getStorage('entity_clone_entity_setting');
    $clone_setting_items = $clone_settings_storage->loadByProperties(['entity_type' => $entity->getEntityTypeId(), 'bundle' => $entity->bundle()]);
    if (!empty($clone_setting_items)) {
      return reset($clone_setting_items);
    }
  }

  /**
   * @return bool
   */
  public static function existsForEntityTypeId(string $entity_type_id) {
    $clone_settings_storage = \Drupal::entityTypeManager()->getStorage('entity_clone_entity_setting');
    $clone_setting_items = $clone_settings_storage->loadByProperties(['entity_type' => $entity_type_id]);
    return !empty($clone_setting_items);
  }

}
