<?php

namespace Drupal\entity_clone_multiple\Controller;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\entityqueue\EntityQueueInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\entityqueue\EntitySubqueueInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\entityqueue\EntityQueueRepositoryInterface;
use Drupal\entity_clone_multiple\Entity\EntityCloneSetting;

/**
 * Returns responses for Entityqueue UI routes.
 */
class EntityCloneUIController extends ControllerBase {

  use AjaxHelperTrait;

  /**
   * The Entityqueue repository service.
   *
   * @var EntityQueueRepositoryInterface
   */
  // protected $entityQueueRepository;

  /**
   * Constructs a EntityQueueUIController object
   *
   * @param EntityQueueRepositoryInterface $entityqueue_respository
   *   The Entityqueue repository service.
   */
  public function __construct() {
    // $this->entityQueueRepository = $entityqueue_respository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // $container->get('entityqueue.repository')
    );
  }

  /**
   * Provides a list of subqueues where an entity can be added.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   (optional) An entity object.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function recurFormForEntity(RouteMatchInterface $route_match, $entity_type_id = NULL, EntityInterface $entity = NULL) {
    if (!$entity) {
      $entity = $route_match->getParameter($entity_type_id);
    }

    $clone_settings = EntityCloneSetting::loadForEntity($entity);

    $build['form'] = $clone_settings->buildForm();

    return $build;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $route_match->getParameter($entity_type_id);

    $clone_settings = EntityCloneSetting::loadForEntity($entity);

    if ($clone_settings && $clone_settings->access('clone')) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

}
