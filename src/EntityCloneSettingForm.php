<?php

namespace Drupal\entity_clone_multiple;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Form handler for Entity Clone Setting forms.
 *
 * @internal
 */
class EntityCloneSettingForm extends EntityForm {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs the EntityCloneSettingForm object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $clone_settings = $this->entity;

    if ($this->operation == 'add') { // Add form
      $form['#title'] = $this->t('Create an entity clone settings');
      $entity_type_definitions = $this->entityTypeManager->getDefinitions();
      $entity_types_list = [];
      foreach($entity_type_definitions as $entity_name => $entity_definition) {
        if ($entity_definition instanceof \Drupal\Core\Entity\ContentEntityType) {
          $entity_types_list[$entity_name] = (string) $entity_definition->getLabel();
        }
      }

      $form['entity_type'] = [
        '#title' => $this->t('Entity Type'),
        '#type' => 'select',
        '#options' => $entity_types_list,
        '#description' => $this->t('The human-readable name of this content type. This text will be displayed as part of the list on the <em>Add content</em> page. This name must be unique.'),
        '#required' => TRUE,
        '#ajax' => [
          'callback' => [$this, 'provideBundleSelection'],
          'event' => 'change',
          'wrapper' => 'ecsf-bundle-selection',
          'method' => 'replaceWith',
        ],
      ];

      $user_input = $form_state->getUserInput();
      $bundle_options = [];

      if (isset($user_input['entity_type'])) {
        $bundle_info = \Drupal::service("entity_type.bundle.info")->getBundleInfo($user_input['entity_type']);
        foreach ($bundle_info as $bundle_name => $info) {
          $bundle_options[$bundle_name] = $info['label'];
        }
      }

      $form['bundle'] = [
        '#title' => $this->t('Bundle'),
        '#type' => 'select',
        '#options' => $bundle_options,
        '#description' => $this->t('The human-readable name of this content type. This text will be displayed as part of the list on the <em>Add content</em> page. This name must be unique.'),
        '#required' => TRUE,
        '#prefix' => '<div id="ecsf-bundle-selection">',
        '#suffix' => '</div>',
      ];
    }
    else { // Edit form
      $bundle_info = \Drupal::service("entity_type.bundle.info")->getBundleInfo($clone_settings->getType())[$clone_settings->getBundle()];
      $entity_type_definition = $this->entityTypeManager->getDefinition($clone_settings->getType());

      $form['#title'] = $this->t('Edit clone settings for %bundle of %entity_type entity type', ['%entity_type' => $entity_type_definition->getLabel(), '%bundle' => $bundle_info['label']]);

      $form['recur_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Date Field'),
        '#options' => $this->getDateFieldOptions($clone_settings->getType(), $clone_settings->getBundle()),
        '#default_value' => $clone_settings->getRecurField(),
        '#required' => TRUE,
        '#description' => $this->t('Select a datetime field here. The cloning will be done by applying duration on this field.')
      ];

      if (isset($user_input['recur_field'])) {
        $current_recur_field = $user_input['recur_field'];
      }
      else {
        $current_recur_field = $clone_settings->getRecurField();
      }

      $form['excluded_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Exclude Fields'),
        '#description' => $this->t('Select fields to be excluded when creatig the clones.'),
        '#options' => $this->getExcludeFieldOptions($clone_settings->getType(), $clone_settings->getBundle(), $current_recur_field),
        '#default_value' => $clone_settings->getExcludedFields() ?? [],
        '#required' => TRUE,
      ];
    }

    return $form;
  }

  /**
   * AJAX callback to update bundle selection list.
   */
  public function provideBundleSelection($form, $form_state) {
    return $form['bundle'];
  }

  /**
   * Utility function to build list of options for the recur field
   */
  public function getDateFieldOptions($entity_type, $bundle_name) {
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle_name);
    $date_field_options = [];
    foreach ($fields as $field_name => $field_definition) {
      if ($field_definition instanceof \Drupal\field\Entity\FieldConfig && $field_definition->getType() == 'datetime') {
        $date_field_options[$field_name] = $field_definition->getLabel();
      }
    }
    return $date_field_options;
  }

  /**
   * Utility function to build list of options for fields to be excluded.
   */
  public function getExcludeFieldOptions($entity_type, $bundle_name, $current_recur_field) {
    $keys = $this->entityTypeManager->getDefinition($entity_type)->getKeys();

    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle_name);
    $date_field_options = [];
    foreach ($fields as $field_name => $field_definition) {
      if (in_array($field_name, [$keys['id'], $keys['uuid']])) {
        // ID fields are always exluded
        // TODO: make this dynamic.
        continue;
      }
      if ($field_name == $current_recur_field) {
        // Recur field will be manipulated on clonning,
        // Thus no sense to put it in the list.
        continue;
      }
      $date_field_options[$field_name] = $field_definition->getLabel();
    }
    if ($this->moduleHandler->moduleExists('content_moderation') && isset($date_field_options['moderation_state'])) {
      // Content moderation_state from options as we will allow user to decide moderation state to use while cloning.
      unset($date_field_options['moderation_state']);
    }
    return $date_field_options;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    if ($this->operation == 'add') {
      $actions['submit']['#value'] = $this->t('Create new clone settings');
    }
    else {
      $actions['submit']['#value'] = $this->t('Save clone settings');
    }
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($this->operation == 'add') {
      /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
      $clone_settings = $this->entity;
      $clone_settings->set('id', $clone_settings->getType() . '__' . $clone_settings->getBundle());
      $existing = $this->entityTypeManager->getStorage($clone_settings->getEntityTypeId())->load($clone_settings->id());
      if ($existing) {
        $form_state->setError($form, $this->t('Close setting alrady exist for %label', ['%label' => $clone_settings->label()]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $clone_settings = $this->entity;

    if ($this->operation == 'add') {
      $clone_settings->set('id', $clone_settings->getType() . '__' . $clone_settings->getBundle());
    }

    $status = $clone_settings->save();

    $t_args = ['%name' => $clone_settings->label()];

    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('The settings for %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('The settings for %name has been added.', $t_args));
      $context = array_merge($t_args, ['link' => $clone_settings->toLink($this->t('View'), 'collection')->toString()]);
      $this->logger('entity_clone_multiple')->notice('Added clone settings for %name.', $context);
    }

    if ($this->operation == 'add') {
      $form_state->setRedirectUrl($clone_settings->toUrl('edit-form'));
    }
    else {
      $form_state->setRedirectUrl($clone_settings->toUrl('collection'));
    }
  }

}
