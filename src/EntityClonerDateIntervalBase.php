<?php

namespace Drupal\entity_clone_multiple;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\entity_clone_multiple\Entity\EntityCloneSetting;

/**
 * Base class for \DateInterval based of entity cloner plugins
 */
abstract class EntityClonerDateIntervalBase extends EntityClonerBase {
  
  /**
   * Return a duration that can be passed to \DateInterval constructor.
   */
  abstract public function interval() : string;

  public function clone(ContentEntityInterface $entity, EntityCloneSetting $clone_settings, int $until_timestamp) {
    $recur_field_value = $clone_settings->getRecurFieldValue($entity);
    $clones = [];
    while ($recur_field_value->add(new \DateInterval($this->interval()))->getTimestamp() < $until_timestamp) {
      $clones[] = $clone_settings->cloneEntity($entity, $recur_field_value);
    }
    return $clones;
  }
}