<?php

namespace Drupal\entity_clone_multiple\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Entity Clone Form' block.
 *
 * @Block(
 *   id = "entity_clone_form",
 *   admin_label = @Translation("Entity Clone Form"),
 *   category = @Translation("Entity Clone Multiple"),
 *   deriver = "Drupal\entity_clone_multiple\Plugin\Block\EntityCloneBlockDeriver"
 * )
 */
class EntityCloneBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SnippetBlock instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $clone_settings_id = $this->getDerivativeId();
    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $clone_settings = $this->entityTypeManager->getStorage('entity_clone_entity_setting')->load($clone_settings_id);
    $build = $clone_settings->buildForm();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Blocks for disabled snippets won't be registered but we still check access
   * in case there is custom access control implementation for the snippet.
   *
   * @see \Drupal\snippet_manager\Plugin\Block\SnippetBlockDeriver::getDerivativeDefinitions()
   */
  public function blockAccess(AccountInterface $account) {
    $clone_settings_id = $this->getDerivativeId();
    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    $clone_settings = $this->entityTypeManager->getStorage('entity_clone_entity_setting')->load($clone_settings_id);
    return $clone_settings->access('clone', NULL, TRUE);
  }

}
