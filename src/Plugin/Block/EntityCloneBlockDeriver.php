<?php

namespace Drupal\entity_clone_multiple\Plugin\Block;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Creates block plugin definitions for all entity clone blocks.
 */
class EntityCloneBlockDeriver extends DeriverBase implements ContainerDeriverInterface {
  use StringTranslationTrait;

  /**
   * The snippet storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityCloneSettingsStorage;

  /**
   * Constructs a Snippet object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_clone_settings_storage
   *   The entity clone settings storage.
   */
  public function __construct(EntityStorageInterface $entity_clone_settings_storage) {
    $this->entityCloneSettingsStorage = $entity_clone_settings_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('entity_clone_entity_setting')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $clone_settings_entities = $this->entityCloneSettingsStorage->loadMultiple();

    /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */
    foreach ($clone_settings_entities as $clone_settings) {
      $delta = $clone_settings->id();
      $this->derivatives[$delta] = $base_plugin_definition;
      $this->derivatives[$delta]['admin_label'] = $this->t('Clone form for :label', [':label' => $clone_settings->label()]);
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
