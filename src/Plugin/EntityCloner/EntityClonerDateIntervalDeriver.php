<?php

namespace Drupal\entity_clone_multiple\Plugin\EntityCloner;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Config\Config;

/**
 * Creates cloner plugin definitions for all duration configurations.
 */
class EntityClonerDateIntervalDeriver extends DeriverBase implements ContainerDeriverInterface {
  use StringTranslationTrait;

  /**
   * The 'aggregator.settings' config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a Snippet object.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The 'entity_clone_multiple.settings' config.
   */
  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory')->get('entity_clone_multiple.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->getDurations() as $duration) {
      $delta = $duration['duration'];
      $this->derivatives[$delta] = $base_plugin_definition;
      $this->derivatives[$delta]['id'] = $duration['duration'];
      $this->derivatives[$delta]['duration'] = $duration['duration'];
      $this->derivatives[$delta]['title'] = $duration['title'];
      $this->derivatives[$delta]['description'] = $duration['description'];
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

  /**
   * Convert string configs to a structured array.
   */
  public function getDurations() {
    $duration_configs = $this->config->get('dateinterval_durations');

    $durations = [];
    foreach (explode("\n", $duration_configs) as $line) {
      list($duration, $title, $description) = explode('|', $line);
      $durations[] = [
        'duration' => $duration,
        'title' => $title,
        'description' => $description,
      ];
    }

    return $durations;
  }

}
