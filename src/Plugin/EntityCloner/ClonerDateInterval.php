<?php

namespace Drupal\entity_clone_multiple\Plugin\EntityCloner;

use Drupal\entity_clone_multiple\EntityClonerDateIntervalBase;

/**
 * Provides a 'Entity Clone Form' block.
 *
 * @EntityCloner(
 *   title = @Translation("Each Duration"),
 *   description = "Clone an entity to repeate each duration",
 *   deriver = "Drupal\entity_clone_multiple\Plugin\EntityCloner\EntityClonerDateIntervalDeriver"
 * )
 */
class ClonerDateInterval extends EntityClonerDateIntervalBase {
  
  public function interval(): string {
    return $this->getPluginDefinition()['duration'];
  }
}