<?php

namespace Drupal\entity_clone_multiple;

use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for most of entity cloner plugins
 */
abstract class EntityClonerBase extends PluginBase implements EntityClonerInterface {
}