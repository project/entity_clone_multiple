<?php

namespace Drupal\entity_clone_multiple;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of EntityCloneSetting entities.
 *
 * @see \Drupal\entity_clone_multiple\Entity\EntityCloneSetting
 */
class EntityCloneSettingListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = t('Name');
    // $header['description'] = [
    //   'data' => t('Description'),
    //   'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    // ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];
    // $row['description']['data'] = ['#markup' => $entity->label()];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    // Place the edit operation after the operations added by field_ui.module
    // which have the weights 15, 20, 25.
    if (isset($operations['edit'])) {
      $operations['edit']['weight'] = 30;
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('No clone setings available. <a href=":link">Add a new clone setting</a>.', [
        ':link' => Url::fromRoute('entity_clone_multiple.entity_clone_entity_setting_add')->toString(),
      ]);
    return $build;
  }

}
