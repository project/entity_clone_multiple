<?php

namespace Drupal\entity_clone_multiple\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an entity cloner annotation object.
 *
 * Plugin Namespace: Plugin\EntityCloner
 *
 * For a working example, see \Drupal\entity_clone_multiple\Plugin\EntityCloner\Week
 *
 * @see \Drupal\entity_clone_multiple\ClonerManager
 * @see \Drupal\entity_clone_multiple\EntityClonerInterface
 * @see plugin_api
 * @see hook_entity_cloner_info_alter()
 *
 * @Annotation
 */
class EntityCloner extends Plugin {

  /**
   * The entity cloner plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the entity cloner plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description of the entity cloner plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

}
