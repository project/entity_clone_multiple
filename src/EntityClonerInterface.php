<?php

namespace Drupal\entity_clone_multiple;

/**
 * Defines the common interface for all Entity Cloner classes.
 *
 * @see \Drupal\entity_clone_multiple\ClonerManager
 * @see \Drupal\entity_clone_multiple\Annotation\Archiver
 * @see plugin_api
 */
interface EntityClonerInterface {

  /**
   * Creates clones of the given entity until the given timestamp
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *  The entity to be clone.
   * @param \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings
   *  The clone settings entity
   * @param int $until_timestamp
   *  The timestamp until the clonning to be perfomed while incrementing the recure datetime field value.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   Array of entities cloned from passed entity
   */
  public function clone(\Drupal\Core\Entity\ContentEntityInterface $entity, \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings, int $until_timestamp);

}
