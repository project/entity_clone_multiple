<?php

namespace Drupal\entity_clone_multiple;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for each media type.
 */
class EntityCloneMultiplePermissions implements ContainerInjectionInterface {
  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MediaPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Returns an array of media type permissions.
   *
   * @return array
   *   The media type permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function entityClonePermissions() {
    // Generate media permissions for all media types.
    $clone_settings_entities = $this->entityTypeManager->getStorage('entity_clone_entity_setting')->loadMultiple();
    $permissions = [];
    foreach ($clone_settings_entities as $clone_settings) {
      /** @var \Drupal\entity_clone_multiple\Entity\EntityCloneSetting $clone_settings */

      $permissions['clone ' . $clone_settings->id()] = [
        'title' => $this->t('Clone %bundle of %entity_type', ['%bundle' => $clone_settings->getBundle(), '%entity_type' => $clone_settings->getType()])
      ];
    }
    return $permissions;
  }

}
